# Patchwork µFramework

!!! danger
    Currently this framework should be considered as Proof-of-Concept.

Patchwork is a simple asynchronous microframework to create distributed multi-services
queue oriented complex applications. Sounds complicated but this framework is 
a minimal codebase to start building application founded on microservices in
modern Python. Each service has well-defined and exclusive resource ownership.
All updates are done via events transported by 3rd party message broker like RabbitMQ or
Apache Kafka. Only retrieve operation can be exposed directly by microservice and 
is allowed to be implemented with direct service - service communication.

## Key features

* **Small**, install only what you need, thin clients also available
* **Modern**, no Python 2, full support for asynchronous Python
* **Asynchronous**, everything happens asynchronously, no waiting by design
* **Easy to code**, just implement processor for your task in the worker, don't bother
about serialization, task lost, retries, logging... everything is done and ready to use.
* **Easy to deploy**, add thin client to existing code and setup some workers deploying
them with your favourite message transport - Kafka, RabbitMQ or Redis.
* **Rewind and retry**, your worker goes down? Rewind task queue and retry all
messages without losing any information
* **No data loss**, transactional events handling plus proper transport software
configuration ensures that your deployment is *really near* no data loss.
* **Assumptions**, this framework is not only the code, there are also some assumptions
and rules which must be satisfied to keep code maintainable