As in most use cases Patchwork node is used just as a runner for custom 
processors, so there is official ready to run docker image available.

To start using Patchwork Node in Docker pull latest image:

```shell
docker pull python-patchwork-node:latest
```

and run:

```shell
docker run python-patchwork-node \
    -v /path/to/processors:/opt/patchwork/processors
```

All processors to load should be placed in `/opt/patchwork/processors`
directory which can be bundled into custom image or mounted. Container
is exposing HTTP rediness and liveness probe at port `5555` on `/rediness`
and `/liveness` path respectively.

Configuration file is stored in `/opt/patchwork/patchwork-settings.json` file
and should be overridden if any more advanced customization is needed.

To run patchwork in debug mode pass `--debug` argument 
