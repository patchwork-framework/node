# Exception handlers

During task processing some exceptions might be raised. It's allowed to add some exception handlers
to act accordingly. The default exception handler retries task explicitly on every exception.

!!! warning
    Explicit retry means that task is duplicated and scheduled again using **worker** publisher. This may
    lead to some unexpected behaviour or side effects.

    [Read more about retrying](#retrying)

## Registering exception handlers

To add new exception handler use `exception_handler` decorator on the worker instance.

!!! note
    Exception handler **must** be a coroutine function.


```python
from patchwork.node import PatchworkWorker
from patchwork.core import Task

worker = PatchworkWorker()


@worker.exception_handler(ValueError)
async def my_exception_handler(task: Task, exception: Exception):
    ...    

```

Exception handler takes two arguments:
* **task** which is an instance to task which processing failed
* **exception** which holds caught exception

The `exception_handler` registers handler for all exception instances for given class, and it's subclasses.

## Exception handler invocation order

If multiple exception handlers match caught exception their execution is ordered by following principles:

#### 1) handler which is registered to the nearest parent class has higher priority

Let's assume that there are two custom handlers registered:
* `value_error_handler` for `ValueError` class
* `generic_error_handler` for `Exception` class

If `ValueError` has been caught (or it's subclass instance) then regardless of the order of exception
handlers registration `value_error_handler` will always be invoked firstly, because it's declared type
is the nearest parent of the caught exception.


#### 2) handler which is registered later is executor earlier

If there are multiple handlers registered for the same exception class, the last registered one is the
first one which will be invoked. This way the default handler which retries tasks is always called last.

## Task Control Exceptions in exception handlers

Custom exception handler may be used to control what should be done with the task in case of the exception.
It's allowed to raise one of [Task Control Exceptions](#task-control-exceptions) to override default
explicit retry behaviour. If exception handler raises one of Task Control Exception no more handlers
will be invoked.

!!! hint
    *For advanced users*

    It's not exactly true that after Task Control Exception no more handlers are called.
    Read [Idempotent exception handlers](#idempotent-exception-handlers) for more details.

## Idempotent exception handlers

Idempotent exception handler is a handler which **can't** raise [Task Control Exceptions](#task-control-exceptions).
Also, there should be no other side effects which may influence how executor works. It should be safe for application
logic to call idempotent handler multiple times.

These handlers were introduced mostly not to *handle* exception but to *report* them. Idempotent handler
can be used for instance to report any processing exception to external reporting tool like Sentry. With regular
handler to make sure that all exception are reported this handler should be registered to be called before
any handler which take some action on the task using Task Control Exceptions. It might be hard to achieve
as more specific and later registered handlers have higher priority.

To mitigate this, handler may be marked as idempotent by settings `idempotent` argument to `True` on 
`exception_handler` decorator. As there is expected that such handler has no
effect on task flow logic, exception processor can safely schedule them even if task processing
has been completed.

!!! example
    ```python
        from patchwork.node import PatchworkWorker
        from patchwork.core import Task

        worker = PatchworkWorker()


        @worker.exception_handler(Exception)
        async def report_exception_handler(task: Task, exception: Exception):
            # this handler will be called always, even if exception has been handled
            # and task has been finalized by one of Task Control Exceptions.
            ...

    ```

!!! danger
    Raising one of Task Control Exception from idempotent handler cause internal executor error and
    leads to its crash.

!!! warning
    It's discouraged to raise any exception from idempotent handlers.

