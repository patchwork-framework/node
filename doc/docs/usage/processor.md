# Processors

Processor is a basic unit which handles task and does some action. It can be considered as a method
in GRPC. Actually processor is a coroutine function which will be executed on the async loop.

!!! important
    Processor should be stateless (but it's not mandatory).

!!! danger
    There is no guarantee that same processor will be scheduled for different (or event same)
    tasks on the same event loop. Depending on used executor and processing unit processor might
    be scheduled in very different way, so make sure that it's not using any global objects or variables
    except dependencies.

!!! hint
    To take power of the async processing make sure that your processors uses async version of
    libraries (eg. asyncpg instead of psycopg)

## First processor
Writing a processor is as easy as writing simple method, so the simplest processor looks like:

```python
async def my_first_processor():
    print("Hello world")
```

... and that's all.


## Routing
Patchwork Node receives tasks from the subscriber and runs processors, however it must know which processor
should be invoked for which task. To solve this, routers were introduced which works very similar to
routers known from web frameworks.

```python

    from patchwork.node import PatchworkWorker
    from patchwork.node.core.router import TaskRouter

    worker = PatchworkWorker()
    router = TaskRouter()

    worker.include_router(router)
```

Every worker application should have at least one task router assigned. It's possible to assign multiple
routers, however tasks are matched to the routers in order of router appending. The task is processed
only by first matched rule.

To register task in the router use router's `on` decorator:

```python

@router.on('hello_world')
async def my_first_processor():
    print("Hello world")
```

Now, the worker knows that for a task which type is `hello_world` `my_first_processor` should be invoked.
To read more about tasks and what *task type* is go to [The Task Chapter](usage/task).

??? example "Full running example"

    ```python
        from patchwork.node import PatchworkWorker
        from patchwork.node.core.router import TaskRouter
    
        worker = PatchworkWorker()
        router = TaskRouter()
    
        worker.include_router(router)

        @router.on('hello_world')
        async def my_first_processor():
            print("Hello world")
        
        worker.main()
    ```
    
## Dependencies (aka arguments)

Our first task was not very useful. Usually tasks have a payload or sends some tasks to another workers, so
they take an arguments and some... dependencies. In Patchwork everything which is passed to the task, regardless
what its actually is we named as a **dependency**. Dependencies are the only proper way to give processors
data and context to execute.

!!! warning
    It's undefined for the processor how it will be run, so avoid any global variables. Processor should
    never use variables out of its scope, everything what is needed to process the task should be 
    passed using dependencies.

**What is this magic dependency?**

Dependency is just an argument for the processor function. Or maybe not just, it looks like a regular argument
but under the hood Patchwork node is doing some magic to make sure that these arguments will work regardless
of processing unit.

### Task payload

Let's extend first processor example to take task payload:

```python
from typing import Optional
from pydantic import BaseModel

class DataPayload(BaseModel):
    message: Optional[str]

@router.on('hello_world')
async def my_first_processor(payload: DataPayload):
    print(payload.message or "Hello world")    
```

Firstly, we need a [Pydantic](https://pydantic-docs.helpmanual.io/) model which represents task payload. 
To get payload it's enough to add `payload` argument to the processor signature. That's all.
Type annotation must points to Pydantic `BaseModel` subclass. If no verification should be done, or
you'd like to process any payload declare a model with 
[`extra` flag in its Config enabled](https://pydantic-docs.helpmanual.io/usage/model_config/).

!!! info
    It's possible to pass multiple payload types, for instance multiple models which reflects
    multiple versions of the communication protocol. `typing.Union` is your friend.


### Task payload field

Sometimes whole task payload is not needed or processor is using just a narrow subset of the payload which
has multiple versions and defining its type would be difficult. To get a payload field just declare
a regular argument. Patchwork will try to get field of the argument name from the payload. If argument
type is given than it will be verified if received data type matches expected data type given in the
processor signature.

So, the payload example will look now as follows:

```python
from typing import Optional
from pydantic import BaseModel

class DataPayload(BaseModel):
    message: Optional[str]

@router.on('hello_world')
async def my_first_processor(message: str):
    print(message or "Hello world")    
```

It's possible to grab payload and some payload fields simultaneously, if needed.
```python
@router.on('hello_world')
async def my_first_processor(message: str, payload: DataPayload):
    print(message or "Hello world")    
```

### Task instance itself

Sometimes processor may need task instance itself, not just a payload. For instance some information
from the task metadata. To get a task just add `task` argument.

```python
@router.on('hello_world')
async def my_first_processor(task: Task):
    print(message or "Hello world")    
```

### Getting a publisher
Sometimes task needs to send another tasks. If worker allows for that, the publisher is configured and
it can be retrieved by the task using `GetPublisher` dependency. In this case the name of argument
doesn't care. 

```python
@router.on('hello_world')
async def my_first_processor(publisher: AsyncPublisher = GetPublisher()):
    await publisher.send(...)
```

### Naming conflicts

Some readers may start thinking what happens if my payload has `task` or `payload` field, and I want
to get it directly? How the framework knows if `payload` argument is a payload or field named `payload` in 
my payload?

It's detected by the declared expected type. If type of the argument is Pydantic `BaseModel` class
framework assumes that it's task payload. Otherwise, it will be considered as a field named `payload` in the
task payload.

Same applies to argument `task`.

!!! hint
    For advanced users.

    Under the hood, framework is converting type annotations to appropriate dependency class instances,
    which resolves to expected object. If needed it's possible to use these dependencies to explicit
    get payload, task or payload field.

    ```python
    from patchwork.node.core.dependencies import GetPayload, GetPayloadField

    async def processor(custom_payload_arg: PayloadModel = GetPayload(), payload_field: Any = GetPayloadField('payload')):
        ...
    ```

    More about dependency system details can be found [here](advanced/dependencies)


### Writing custom dependencies
TODO:


## Exceptions handling

If processor expects some exceptions and knows what should be done it's better to handle exception
internally in the processor. All unhandled exceptions will be passed up to the executor and
handled by [global exception handlers](usage/exc_handlers).

There is a special family of exceptions named [Task Control Exceptions](usage/tce) which allows to control
what to do with the task in case of failure.

Use them to retry task, drop it or mark as permanently failed, which means that shouldn't be retried.