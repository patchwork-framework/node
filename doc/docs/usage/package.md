In more complex application or when advanced usage is needed Patchwork Node
can be setup and run as a part of larger application. 

The root class of the node is `Worker`. To create worker just instantiate it:

```python
from patchwork.node import PatchworkWorker

worker = PatchworkWorker()
```

!!! warning
    Currently worker can be instantiated only in process main thread.
    
Instantiated worker is ready to run. There is blocking and non-blocking way
to start the worker. To start it and wait call `run()` method which
stats local asyncio loop, setup signals, executes `start()` job and waits
for  `SIGTERM` or `SIGINT` signals.

If your application has already asyncio loop configured and Patchwork Node
is just a component, worker can be started by calling `start()` method.
To stop worker use `stop()` method. To monitor worker and get notified when
stopped (or crashed) await on `wait_for(False)` method in `state` worker property.

Example:
```python
from patchwork.node import PatchworkWorker

worker = PatchworkWorker()

# somewhere in async background task
await worker.run()
# Worker has started
await worker.state.wait_for(False)
# Worker stop running

# ... and somewhere in stop/application terminate coroutine
await worker.terminate()

```