# Task Control Exceptions (TCE)

Patchwork provides set of exceptions which should be used by the application logic
to control what should be done with current task if it can't be processed with 
success.


- `TaskRetry`, when raised forces executor to re-send the task. Task is send as
    new one with the same payload and metadata. Optional `countdown` argument is 
    a number of seconds which must left before task can be executed.
    
    Current task is committed.
    
    This exception is useful if task is communicating with rate limited 3rd party
    service. When limit is exceeded processor may raise `TaskRetry` exception
    with given `countdown` to avoid external service overload.
    
    The second main use case are long-running tasks. To avoid blocking executor
    for too much time, processor may stop doing the job at some well-defined
    point and store progress using exception `progress` argument. Value can
    be any number, string or bytes. 
  
- `TaskFatal`, this exception should be raised when given task is unprocessable
    now and in the future and should not be retried. Optional argument `reason`
    should be used to pass details.
  
    The main use case is when processor is communicating with external service
    and such service is telling that payload is invalid or when internal
    processor task payload validation failed.
  
- `TaskDrop`, raise when task should be just dropped. The main use case is that
  task become obsolete.


## Override default TCE behaviour

!!! danger
    This topic is for advanced users only.

At the beginning please consider that changing the default behaviour may lead to data
loss or soften the at least one delivery guarantee of Patchwork. Make sure that you really
know what you are doing.

As mentioned in the [chapter about exception handlers](#exceptions-handlers) custom handlers
might be registered. The default exception handler matches also Task Control Exceptions and
just re-raises them, so the case is the same as when custom handler raises one of TCEs.
Any TCEs raises during task processing is then re-raised at finalization stage when exception
handlers are invoked. So, behaviour for TCEs raised during task processing may be overridden
just by registering custom exception handler which matches one or more TCEs. 

However, it's not possible to change behaviour of TCEs raised in exception handlers.
