Patchwork Node configuration is stored current working directory 
`patchwork-settings.json` file.
Default file and patch can be changed using `settings` argument in command line
or `Worker.__init__` method. 

If settings file does not have `settings_locked` option it's also allowed
to pass settings options as environment variables. Name should be the same
as option name from settings file prepended by `PATCHWORK_NODE_`, eg.
`PATCHWORK_NODE_DEBUG=1` will override `debug` property value from settings file.

!!! warning
    On **production** it's highly recommended to set `settings_locked` option 
    in the file to `true` to avoid reconfiguration via environment variables.
    
    If `settings_locked` option is missing it's considered as `true` which means
    overridden is forbidden. 
    
!!! warning 
    Passing configuration options using environment variables is discouraged on
    production. Values may contain sensitive information which can be compromised
    by Docker (cloud) inspection tools.
    
## Options

### `clients`

Clients are connectors to the message broker. Node may work with multiple 
clients at the same time. Each client is identified by unique name, so
`clients` options is a dictionary where key is a client name and value
is a client configuration. Client configuration is dictionary with mandatory
`engine` key and optional `options` field. `engine` is a client module path
and class name joined by a colon. `options` are passed as client `__init__` method
arguments and their actual structure depends on the client class.

Example:
```json
{
...
"clients": {
  "default": {
     "engine": "some.client:ClientClass",
     "options": { ... additional client options ...}
  }
},
...
}
```

It is possible to instantiate same client class multiple time. Only client name
must be unique. 

!!! note
    At least one client with name `default` should be defined. This client is 
    used by default in all processors to send tasks. Without this processors
    must pass client name explictly or do not send tasks at all (which is fine
    in some use cases).


### `debug`

If set Patchwork node will be working in debug mode which means that more assertions
will be checked. This option does not affect logging levels.

### `executor`

Executor is Patchwork Node module which is responsible of listening on clients and
passing tasks to processing unit. The value is a dictionary which must has `engine`
key set and may has optional `options` property defined.

`engine` is a path to the executor module and class joined with colon.

```json
{
  ...
  "executor":  {
    "engine": "patchwork.node.executor:DefaultExecutor",
    "options": { ... additional options ...}
  },
  ...
}
```

`options` value is a dictionary with executor configuration which depends on
actual executor engine.

By default `patchwork.node.executor:DefaultExecutor` is used which has following options:

* `terminate_timeout`, number of seconds to wait for running jobs to stop when
   termination is requested. If jobs won't stop in given timeout are killed.
* `listen_on`, list of client names to listen on (see [clients](#clients) option)
* `unit`, processing unit for this executor which is a dict with `engine` key
    required and optional `options` key. Engine is a path to the procesing unit
    module and class joined by colon, `options` value is a dictionary.
     
        {
        ...
        "executor": {
         "engine": "patchwork.node.executor:DefaultExecutor",
         "options": {
           "unit": {
             "engine": "patchwork.node.executor.unit:InlineUnit",
             "options": { ... additional unit options ... }
           }
         }
        }
        ...
        }
  
    For more information about processing units and their options see 
    [Processing Units](modules.md#processing-unit)

### `logging`

Configures Patchwork logging. Each component has own logger named as 
`patchwork.node.[component]` and can be configured independently. Value should
be a logging configuration dictionary expected by 
[`basicConfig`](https://docs.python.org/3/library/logging.html#logging.basicConfig) method
from Python `logging` package. 

By default all loggers run at `DEBUG` level and prints messages to the standard output.

!!! warning
    Log messages should not contain sensitive information, but it can't be guaranteed as
    external libraries are also writing to the log. Consider logging output as confidential.

### `manager`

Manager module allows to maintain Patchwork Node remotely. By default `HttpManager`
is used.

!!! danger
    Manager should be accessible only over internal network. Patchwork framework
    is not providing any kind of authorization in default `HttpManager` implementation
    which is also not secured by SSL.
    
    It's expected that manager port is not exposed on public network or if it's done
    via proxy responsible of authorization and encryption.

The value of manager options is a dictionary with mandatory `engine` key and optional
`options` field. `engine` is a path to the manager module and class joined by a colon.

```json
{
...
"manager": {
  "engine": "patchwork.node.manager:HttpManager",
  "options": {
    "port": 5555
  }
}
}
```

Additional manager options can be passed in `options` dictionary. `HttpManager` class
has one configurable option `port`.

For more managers and their options see [Managers](modules.md#managers).

### `modules`

Modules option is a list of additional modules which should be started together with
the worker. These modules may provide additional functionality, monitoring or caching.
Each module enty must be a dictionary with mandatory `engine` key and optional `options`
field. `engine` is a module path and class name joined by a colon.

More information about modules can be found in [Modules](modules.md#modules).

### `processors`

List of processors to load. Each list entry is a dictionary with mandatory `engine`
key and optional `options` field. `engine` is a processor module path and class name
joined by a colon. `options` structure depends on actual processor class, but all
values are passed to the processor `__init__`.

To load processors from given path or package use `patchwork.node:DiscoverProcessors`
as an engine pass in options either:

* `start_dir` property with absolute path to the directory which should be scanned
* `package` with python path to the package which should be scanned

Additionally if scan should be recursive pass `recursive` option set to `1`.

Example:
```json
{
...
"processors": [
  {
    "engine": "patchwork.node:DiscoverProcessors",
    "options": {
       "start_dir": "/opt/patchwork/processors",
       "recursive": 1
    } 
  }
],
...
}
```

All classes which inherits `patchwork.node.core:BaseProcessor` found in given
path or package will be loaded.