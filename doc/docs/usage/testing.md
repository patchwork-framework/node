# Testing

## TestWorker
Patchwork Node provides a `TestWorker` class which can be used to test workers. It's similar to some ASGI TestClient's.

`TestWorker` is a wrapper for real worker. It must be instantiated with a `PatchworkWorker` as the only argument. To 
run worker use it as async context.

```python
from patchwork.node.testutils import TestWorker
from somewhere import app 
# app is an instance of PatchworkWorker

def test_method():
    async with TestWorker(app) as worker:
        ...
```

`__aenter__` of `TestWorker` starts given worker and makes all initializations.

!!! danger
    Make sure that app instance has proper settings for tests!

!!! danger
    `TestWorker` may work only with app that has `AsyncLocalSubscriber` and `AsyncLocalPublisher` used.

!!! hint
    It's recommended to use `InlineUnit` executor for faster tests executions.

`TestWorker` provides a `catch_tasks()` method which grabs all tasks executed by the worker. It blocks
until there is no more tasks to execute by the worker. `TaskCatcher` instance is returned as a result which
provides some assertion methods to validate if expected tasks were executed.

```python
from patchwork.node.testutils import TestWorker
from somewhere import app 
# app is an instance of PatchworkWorker

def test_method():
    async with TestWorker(app) as worker:
        await app.get_publisher().send(b'test data', queue_name='test')
        
        # awaits until worker stops processing and there will be no more tasks on queues
        # subscribed by the application subscriber
        tasks = await worker.catch_tasks()

        ... tasks assertions ...
```

## TaskCatcher

It's a simple utility to grab all tasks executed by a worker executor. As an addition it gives some
assertions for processed tasks.

::: patchwork.node.testutils.worker.TaskCatcher
