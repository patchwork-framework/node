Patchwork Node work with set of standard modules which are mandatory:

* executor
* processing unit
* manager

Additionally some 3rd party modules can be started to add custom worker
wide functionality.

!!! hint
    To get more details of modules and how to write them, see Node Advanced
    topics.

## Executor

## Processing Unit

## Manager

## Modules