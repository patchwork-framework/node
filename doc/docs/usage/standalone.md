The simplest method of Patchwork node deployment is running it as a standalone 
Python process. To do that just install `patchwork-node` package and call
`patchwork run` command. 

By default Patchwork node is running in `production` mode, use standard output
as logger and scan current working directory for task processors. Each Python class
found which has `process()` method declared is considered as processor. 

`run` command takes some optional arguments:

* `--debug`, run node in `debug` mode
* `--settings`, path to custom settings file, by default Patchwork Node is looking
   for `patchwork-settings.json` file in current working directory
* `--processor`, which is a processor class to load. To load multiple processors
   pass this argument multiple times. Discover and explicite processor paths
   can be mixed.
   
   Processor path must be a string of Pythonic module path and class name
   join by colon. Example: `foo.bar:ProcessorClass`. 
   
   To load all processors (classes) from given file pass `*` as a class name, 
   eg: `foo.bar:*`.
   
   By default processors are imported and instantiated without any arguments,
   to pass arguments append them to the path in JSON-compatible form:
   `{"arg1": "value", "arg2": "value"}`. All argument values are passed as `str`.
   Example: `foo.bar:ProcessorClass{"arg": "value"}`   
   
   To discover processors in given path or package and load all modules and all
   processors set `patchwork.node:DiscoverProcessors`
   as a processor. By default current working directory is scanned, to scan
   custom one pass `start_dir` argument with an absolute path. To scan
   package pass `package` argument. By default only root of given path or package
   is scanned, to perform recursive scan set `recursive` argument to `1`.
   