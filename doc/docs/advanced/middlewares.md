# Middlewares

When new task arrives to the executor some preparation is done before it's passed to the processing unit.
This operation is done by middlewares. 

Middlewares are called before task routing, at the executor loop, so make sure that all of them are thin as heavy
middlewares may cause slow down of the executor and thus whole worker.

To add new middleware use `add_middleware` decorator on the `PatchworkWorker` instance. Currently only
task processing flow uses middlewares, so the only accepted argument for the decorator is `task` literal.

!!! note
    All middleware functions must be coroutine functions.

```python
    from patchwork.node import PatchworkWorker

    worker = PatchworkWorker()

    @worker.middleware('task')
    async def custom_middleware(task: Task, receiver: AsyncSubscriber, finalizer: FinalizerType):
        ...
```

Middleware takes as an arguments received task, instance of subscriber which received it and a finalizer function.
Finalizer function is called **after** task processing. If middleware expects some action to be done after task
processing it should return new finalizer function. If no function is returned, original finalizer will be used.

!!! warning
    If you'd like to use custom finalizer remember to call passed finalizer so previous middlewares 
    finalization will also be done!

    Remember!

    Finalizer is a coroutine function so **await** on it.

Finalizer is a coroutine function which takes three arguments:
* **task**, which is instance of processed `Task`
* **receiver**, the subscriber which received this task
* **processing_fut**, a resolved `asyncio.Future` which represents a future created for task processing on the 
  processing unit. This future has a result of task processor or exception if any was raised, or it might be cancelled 
  if task on processing using has been cancelled.


```python
    from patchwork.node import PatchworkWorker

    worker = PatchworkWorker()
    
    async def finalizer(task: Task, receiver: AsyncSubscriber, processing_fut: asyncio.Future, parent: FinalizerType):
        ...
        await parent(task, receiver, processing_fut)
    
    @worker.middleware('task')
    async def custom_middleware(task: Task, receiver: AsyncSubscriber, finalizer: FinalizerType):
        ...
        return partial(finalizer, parent=finalizer)
```