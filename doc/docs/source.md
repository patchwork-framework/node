# Patchwork Node

Project source code can be found at [GitLab](https://gitlab.com/patchwork-framework/node).

Package name: `patchwork-node`

This package implements a workhorse of Patchwork. Node is able to fetch tasks from queues, process them and optionally
produce new messages.
Node implementation hides all communication and management details and allows programmer to define
task handlers just as callables.

## Node Core
::: patchwork.node.core

## Executor
::: patchwork.node.executor

## Processing Unit
::: patchwork.node.executor.unit

## Mananger
::: patchwork.node.manager
