# -*- coding: utf-8 -*-

from patchwork.node.core.worker import PatchworkWorker

if __name__ == '__main__':
    worker = PatchworkWorker()
    worker.main()
